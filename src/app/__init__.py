from flask import Flask, jsonify, render_template
from flask_cors import CORS
from app.resources import rectBp, cutBp
from typing import Union, Any, Dict, List


class MyFlask(Flask):
    def make_response(self, rv: Union[List[Any], Dict[Any, Any]]):
        if isinstance(rv, (dict, list)):
            rv = jsonify(rv)
        return super().make_response(rv)


def create_app(config_filename: str = None):
    app: MyFlask = MyFlask(
        __name__, static_folder="../dist/static", template_folder="../dist/"
    )
    cors: CORS = CORS(app, resources={r"/api/*": {"origins": "*"}})  # noqa
    app.config.from_pyfile("config.py")

    @app.route("/")
    def index() -> str:
        return render_template("index.html")

    @app.route("/", defaults={"path": ""})
    @app.route("/<path:path>")
    def catch_all(path: Union[str, bytes]) -> str:
        return render_template("index.html")

    # Return validation errors as JSON
    @app.errorhandler(422)
    @app.errorhandler(400)
    def handle_error(err):
        headers: str = err.data.get("headers", None)
        messages: str = err.data.get("messages", ["Invalid request."])
        if headers:
            return {"errors": messages}, err.code, headers
        else:
            return {"errors": messages}, err.code

    app.register_blueprint(rectBp)
    app.register_blueprint(cutBp)
    return app
