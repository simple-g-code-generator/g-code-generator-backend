from dataclasses import dataclass
from math import ceil
from .base_cnc import BaseMilling as Base
from .base_cnc import start_stop, xfrange


@dataclass
class RectMilling(Base):
    """
    Simple generator of filled rectangular milling, inheritance from BaseMilling
    Methods:
    -------------
    downY()
        downhill milling along  Y axis

    downX()
        downhill milling along X axis

    upY()
        conventional milling along Y axis

    upX()
        conventional milling along X axis

    biX()
        bidirectional milling along X axis

    biY()
        bidirectional milling along Y axis
    """

    WOC: float = 3

    @start_stop
    def biY(self) -> str:
        """bidirectional milling along Y axis
        Two way milling, Conventional and Climb together, no safe Z when switch type of work
        Nested loop when max_depth > 0
        variables:
            - step_z: reformat step_z to actual depth, equal step for loop
            - step_x: reformat step_x to based on WOC and dim_x, equal step for loop
        Return: return Gcode String
        """
        c = self.comds()
        step_z: float = self.depth_max / ceil(
            self.depth_max / self.depth_step
        ) if self.depth_max else 0
        step_x: float = self.dim_x / (ceil(self.dim_x / self.WOC))
        for actual_z in xfrange(step_z, self.depth_max, step_z):
            actual_x: float = 0
            while actual_x <= self.dim_x:
                c(self.working_line(actual_z, actual_x)["downY"])
                actual_x += step_x
                if actual_x > self.dim_x:
                    break
                c(self.working_line(actual_z, actual_x)["upY"])
                actual_x += step_x
            c(self.nonworking_line()["upX"])
            c(self.nonworking_line()["downY"])
        return c("stop")

    @start_stop
    def biX(self) -> str:
        """bidirectional milling along X axis
        Two way milling, Conventional and Climb together, no safe Z when switch type of work
        variables:
            - step_z: reformat step_z to actual depth, equal step for loop
            - step_x: reformat step_x to based on WOC and dim_x, equal step for loop
        Return: return Gcode String
        """
        c = self.comds()
        step_z: float = self.depth_max / ceil(
            self.depth_max / self.depth_step
        ) if self.depth_max else 0
        step_y: float = self.dim_y / (ceil(self.dim_y / self.WOC))
        for actual_z in xfrange(step_z, self.depth_max, step_z):
            actual_y: float = 0
            while actual_y <= self.dim_y:
                c(self.working_line(actual_z, actual_y)["upX"])
                actual_y += step_y
                if actual_y > self.dim_y:
                    break
                c(self.working_line(actual_z, actual_y)["downX"])
                actual_y += step_y
            c(self.nonworking_line()["upX"])
            c(self.nonworking_line()["downY"])
        return c("stop")

    @start_stop
    def downY(self) -> str:
        """
        Climb rectangular milling along axis Y based on paramaters from user forms, safe Z, one direct of
        working motion

        inner variables:
        - step_z: reformat step_z to actual depth, equal step for loop
        - step_x: reformat step_x to based on WOC and dim_x, equal step for loop

        Returns:
            str -- gcode commands
        """
        c = self.comds()
        step_z: float = self.depth_max / ceil(
            self.depth_max / self.depth_step
        ) if self.depth_max else 0
        step_x: float = self.dim_x / (ceil(self.dim_x / self.WOC))
        for actual_z in xfrange(step_z, self.depth_max, step_z):
            for actual_x in xfrange(0, self.dim_x, step_x):
                c(self.working_line(actual_z, actual_x)["downY"])
                c(self.nonworking_line()["downY"])
                print("test")
            c(self.nonworking_line()["upX"])
            c(self.nonworking_line()["downY"])
        return c("stop")

    @start_stop
    def downX(self) -> str:
        """
        Climb rectangular milling along axis X based on paramaters from user forms, safe Z, one direct of
        working motion

        inner variables:
        - step_z: reformat step_z to actual depth, equal step for loop
        - step_y: reformat step_x to based on WOC and dim_x, equal step for loop

        Returns:
            str -- gcode commands
        """
        c = self.comds()
        step_z: float = self.depth_max / ceil(
            self.depth_max / self.depth_step
        ) if self.depth_max else 0
        step_y: float = self.dim_y / (ceil(self.dim_y / self.WOC))
        for actual_z in xfrange(step_z, self.depth_max, step_z):
            for actual_y in xfrange(0, self.dim_y, step_y):
                c(self.nonworking_line()["downX"])
                c(self.working_line(actual_z, actual_y)["downX"])
            c(self.nonworking_line()["upX"])
            c(self.nonworking_line()["downY"])
        return c("stop")

    @start_stop
    def upX(self) -> str:
        """
        Conventional rectangular milling along axis X based on paramaters from user forms, safe Z, one direct of
        working motion

        inner variables:
        - step_z: reformat step_z to actual depth, equal step for loop
        - step_y: reformat step_x to based on WOC and dim_x, equal step for loop

        Returns:
            str -- gcode commands
        """
        c = self.comds()
        step_z: float = self.depth_max / ceil(
            self.depth_max / self.depth_step
        ) if self.depth_max else 0
        step_y: float = self.dim_y / (ceil(self.dim_y / self.WOC))
        for actual_z in xfrange(step_z, self.depth_max, step_z):
            for actual_y in xfrange(0, self.dim_y, step_y):
                c(self.working_line(actual_z, actual_y)["upX"])
                c(self.nonworking_line()["upX"])
            c(self.nonworking_line()["upX"])
            c(self.nonworking_line()["downY"])
        return c("stop")

    @start_stop
    def upY(self) -> str:
        """
        Conventional rectangular milling along axis Y based on paramaters from user forms, safe Z, one direct of
        working motion

        inner variables:
        - step_z: reformat step_z to actual depth, equal step for loop
        - step_x: reformat step_x to based on WOC and dim_x, equal step for loop

        Returns:
            str -- gcode commands
        """
        c = self.comds()
        step_z: float = self.depth_max / ceil(
            self.depth_max / self.depth_step
        ) if self.depth_max else 0
        step_x: float = self.dim_x / (ceil(self.dim_x / self.WOC))
        for actual_z in xfrange(step_z, self.depth_max, step_z):
            for actual_x in xfrange(0, self.dim_x, step_x):
                c(self.nonworking_line()["upY"])
                c(self.working_line(actual_z, actual_x)["upY"])
            c(self.nonworking_line()["upX"])
            c(self.nonworking_line()["downY"])
        return c("stop")
