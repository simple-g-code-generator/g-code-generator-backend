from .base_cnc import BaseMilling, start_stop, xfrange
from dataclasses import dataclass
from math import ceil


@dataclass
class Cutting(BaseMilling):
    is_ramping: bool = True
    _max_angle: float = 10
    length_ramp: float = 10

    def ramping_y(self, actual_z, step_z):
        p = self.offset()
        c = self.comds()
        c(f"G0Z{-actual_z+step_z+0.1}")
        c(
            f"G1X{p[0]:.3f}Y{self.length_ramp:.3f}Z{-actual_z+step_z/2:.3f}F{self.feed/2:.3f}"
        )
        c(f"G1X{p[0]:.3f}Y{p[1]:.3f}Z{-actual_z:.3f}F{self.feed/2:.3f}")
        return c("stop")

    def ramping_x(self, actual_z, step_z):
        p = self.offset()
        c = self.comds()
        c(f"G0Z{-actual_z+step_z+0.1}")
        c(
            f"G1X{self.length_ramp:.3f}Y{p[3]:.3f}Z{-actual_z+step_z/2:.3f}F{self.feed/2:.3f}"
        )
        c(f"G1X{p[0]:.3f}Y{p[3]:.3f}Z{-actual_z:.3f}F{self.feed/2:.3f}")
        return c("stop")

    @start_stop
    def cut(self):
        c = self.comds()
        step_z: float = self.depth_max / ceil(
            self.depth_max / self.depth_step
        ) if self.depth_max else 0
        line = self.working_line
        p = self.offset()
        if self.dim_y < 50 and self.dim_y < self.dim_x * 0.5:
            c(self.nonworking_line()["upY"])
            for actual_z in xfrange(step_z, self.depth_max, step_z):
                if self.is_ramping:
                    c(self.ramping_x(actual_z, step_z))
                c(line(actual_z, p[3])["upX"])
                c(line(actual_z, p[2])["upY"])
                c(line(actual_z, p[1])["downX"])
                c(line(actual_z, p[0])["downY"])

        else:
            for actual_z in xfrange(step_z, self.depth_max, step_z):
                if self.is_ramping:
                    c(self.ramping_y(actual_z, step_z))
                c(line(actual_z, p[0])["downY"])
                c(line(actual_z, p[3])["upX"])
                c(line(actual_z, p[2])["upY"])
                c(line(actual_z, p[1])["downX"])
        return c("stop")


if __name__ == "__main__":
    x = Cutting(depth_step=0.5, depth_max=6.7)
    with open("test", "w") as f:
        f.write(x.cut())
        f.close()
