import uuid
from dataclasses import dataclass
from functools import wraps
from typing import Callable, List, Optional, Tuple, Iterator, Dict


def xfrange(start: float, stop: float, step: float) -> Iterator[float]:
    i = 0
    while start + i * step < stop:
        yield start + i * step
        i += 1
    else:
        yield stop


def start_stop(func: Callable) -> Callable:
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        c: callable = self.comds()
        c(self.start())
        c(func(self, *args, **kwargs))
        c(self.end())
        return c("stop")

    return wrapper


@dataclass
class BaseMilling(object):
    dim_x: float = 10
    dim_y: float = 10
    safe_z: float = 2
    dim_tool: float = 6
    spindle: int = 24000
    feed: float = 2000
    depth_max: float = 2
    depth_step: float = 1
    offset_x: float = 0
    offset_y: float = 0
    tool_offset: str = "inline"

    def comds(self) -> Callable:
        commands: List[str] = list()

        def comd(s: str) -> Optional[str]:
            if s == "stop":
                return "\n".join(commands)
            commands.append(s)
            return None

        return comd

    def start(self) -> str:
        c = self.comds()
        p = self.offset()
        c("%\n( milling by Jacek S. )")
        c("O0001")
        c("G54")
        c("G17")
        c("G00 G90")
        c(f"G0Z{self.safe_z:.3f}")
        c(f"G0X{p[0]}Y{p[1]}")
        c(f"S{self.spindle}M3")
        return c("stop")

    def end(self) -> str:
        c = self.comds()
        c(f"G0Z{self.safe_z:.3f}")
        c(f"G0X0Y0")
        c(f"M30")
        c("%\n")
        return c("stop")

    def offset(self) -> Tuple[float, float, float, float]:
        offset_dict = dict(inline=0, outside=-self.dim_tool / 2, inside=self.dim_tool / 2)
        x1: float = 0 + self.offset_x + offset_dict[self.tool_offset]
        y1: float = 0 + self.offset_y + offset_dict[self.tool_offset]
        x2: float = self.dim_x - offset_dict[self.tool_offset]
        y2: float = self.dim_y - offset_dict[self.tool_offset]
        return (x1, y1, x2, y2)

    def working_line(self, depth: float, curr: float = 0) -> Dict[str, str]:
        p: Tuple[float, float, float, float] = self.offset()
        line: Dict[str, str] = {}
        line[
            "downX"
        ] = f"G1Z-{depth:.3f}F{self.feed:.3f}\nG1Y{curr:.3f}\nG1X{p[0]:.3f}F{self.feed:.3f}"
        line[
            "upX"
        ] = f"G1Z-{depth:.3f}F{self.feed:.3f}\nG1Y{curr:.3f}\nG1X{p[2]:.3f}F{self.feed:.3f}"
        line[
            "downY"
        ] = f"G1Z-{depth:.3f}F{self.feed:.3f}\nG1X{curr:.3f}\nG1Y{p[3]:.3f}F{self.feed:.3f}"
        line[
            "upY"
        ] = f"G1Z-{depth:.3f}F{self.feed:.3f}\nG1X{curr:.3f}\nG1Y{p[1]:.3f}F{self.feed:.3f}"
        return line

    def nonworking_line(self) -> Dict[str, str]:
        p: Tuple[float, float, float, float] = self.offset()
        line: Dict[str, str] = {}
        line["downX"] = f"G1Z{self.safe_z:.3f}F{self.feed:.3f}\nG0X{p[2]:.3f}"
        line["upX"] = f"G1Z{self.safe_z:.3f}F{self.feed:.3f}\nG0X{p[0]:.3f}"
        line["downY"] = f"G1Z{self.safe_z:.3f}F{self.feed:.3f}\nG0Y{p[1]:.3f}"
        line["upY"] = f"G1Z{self.safe_z:.3f}F{self.feed:.3f}\nG0Y{p[3]:.3f}"
        return line

    @classmethod
    def save_to_file(cls, s: str, root_folder: Optional[str]) -> str:
        path: str = f"{root_folder}/download/{str(uuid.uuid4())}.tap"
        with open(path, "w") as f:
            f.write(s)
            return path
