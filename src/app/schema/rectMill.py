from marshmallow import Schema, fields, validate, validates_schema, ValidationError


class RectMillSchema(Schema):
    dim_x = fields.Float(required=True, validate=validate.Range(min=0))
    dim_y = fields.Float(required=True, validate=validate.Range(min=0))
    safe_z = fields.Float(required=True, validate=validate.Range(min=0.5))
    dim_tool = fields.Float(required=True, validate=validate.Range(min=0.1))
    WOC = fields.Float(required=True, validate=validate.Range(0.1))
    spindle = fields.Int(required=True, validate=validate.Range(1, 60000))
    feed = fields.Float(required=True, validate=validate.Range(min=1))
    depth_max = fields.Float(required=True, validate=validate.Range(min=0))
    depth_step = fields.Float(required=True, validate=validate.Range(min=0))
    offset_x = fields.Float(required=True, validate=validate.Range(min=0))
    offset_y = fields.Float(required=True, validate=validate.Range(min=0))
    tool_offset = fields.Str(
        required=True, validate=validate.OneOf(["inline", "outside", "inside"])
    )
    method = fields.Str(
        required=True,
        validate=validate.OneOf(["downY", "downX", "upY", "upX", "biX", "biY"]),
    )

    @validates_schema
    def validates_woc(self, data, **kwargs):
        if data["WOC"] > data["dim_tool"]:
            raise ValidationError(
                "Width of Cut (WOC) must be smaller or equal to dimension of tool"
            )

    @validates_schema
    def validate_depth(self, data, **kwargs):
        if data["depth_max"] > 0 and data["depth_step"] == 0:
            raise ValidationError(
                "Depth step must be greater than 0 if max depth is greater than 0"
            )
