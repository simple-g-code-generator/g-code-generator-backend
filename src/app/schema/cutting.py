from typing import Dict, Any
from marshmallow.fields import Bool, Float, Int, Str
from marshmallow import Schema, fields, validate, validates_schema, ValidationError


class CutSchema(Schema):
    dim_y: Float = fields.Float(required=True, validate=validate.Range(min=0))
    dim_x: Float = fields.Float(required=True, validate=validate.Range(min=0))
    safe_z: Float = fields.Float(required=True, validate=validate.Range(min=0.5))
    dim_tool: Float = fields.Float(required=True, validate=validate.Range(min=0.1))
    spindle: Int = fields.Int(required=True, validate=validate.Range(1, 60000))
    feed: Float = fields.Float(required=True, validate=validate.Range(min=1))
    depth_max: Float = fields.Float(required=True, validate=validate.Range(min=0))
    depth_step: Float = fields.Float(required=True, validate=validate.Range(min=0))
    offset_x: Float = fields.Float(required=True, validate=validate.Range(min=0))
    offset_y: Float = fields.Float(required=True, validate=validate.Range(min=0))
    tool_offset: Str = fields.Str(
        required=True, validate=validate.OneOf(["inline", "outside", "inside"])
    )
    is_ramping: Bool = fields.Boolean(required=True)
    length_ramp: Float = fields.Float(required=True)

    @validates_schema
    def validates_length(self: Schema, data: Dict[str, Any], **kwargs) -> None:
        if data["depth_step"] / data["length_ramp"] > 0.5:
            raise ValidationError(
                "Ramp angle is too high, increase ramp length or decrease depth step"
            )
        if data["dim_y"] < 50 and data["dim_y"] < data["dim_x"] * 0.5:
            length = data["dim_x"]
        else:
            length = data["dim_y"]
        if data["length_ramp"] > length:
            raise ValidationError(
                "Ramp length can't be greater than the dimension along axis with ramp"
            )

    @validates_schema
    def validate_depth(self: Schema, data: Dict[str, Any], **kwargs) -> None:
        if data["depth_max"] > 0 and data["depth_step"] == 0:
            raise ValidationError(
                "Depth step must be greater than 0 if max depth is greater than 0"
            )
