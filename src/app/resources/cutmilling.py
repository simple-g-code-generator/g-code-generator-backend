import os

from flask import Blueprint, current_app
from flask.views import MethodView
from flask.wrappers import Response
from webargs.flaskparser import use_kwargs
from typing import Union, Tuple, Literal, Dict
from app.gcode import Cutting
from app.schema import CutSchema

cutBp = Blueprint("cutMilling", __name__, url_prefix="/api/v1/")


class Cut(MethodView):
    @use_kwargs(CutSchema())
    def post(self, **kwargs) -> Union[Response, Tuple[Dict[str, str], Literal[500]]]:
        try:
            Mill = Cutting(**kwargs)
            path = Cutting.save_to_file(Mill.cut(), current_app.root_path)

            def generate():
                with open(path) as f:
                    yield from f
                os.remove(path)

            r: Response = current_app.response_class(generate(), mimetype="text/plain")
            r.headers.set("Content-Disposition", "attachment", filename="gcode.tap")
            r.status = "201"
            return r
        except Exception:
            return {"message": "something went wrong"}, 500


cutBp.add_url_rule("/cutmill", view_func=Cut.as_view("CutMill"))

