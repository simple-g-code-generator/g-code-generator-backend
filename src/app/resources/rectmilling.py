import os

from flask import Blueprint, current_app
from flask.views import MethodView
from webargs.flaskparser import use_kwargs

from app.gcode import RectMilling
from app.schema import RectMillSchema

rectBp = Blueprint("rectMilling", __name__, url_prefix="/api/v1/")


class Rect(MethodView):
    @use_kwargs(RectMillSchema())
    def post(self, **kwargs):
        method: str = kwargs["method"]
        del kwargs["method"]
        try:
            Mill = RectMilling(**kwargs)
            path = RectMilling.save_to_file(
                getattr(Mill, method)(), current_app.root_path
            )

            def generate():
                with open(path) as f:
                    yield from f
                os.remove(path)

            r = current_app.response_class(generate(), mimetype="text/plain")
            r.headers.set("Content-Disposition", "attachment", filename="gcode.tap")
            r.status = "201"
            return r
        except Exception:
            return {"message": "something went wrong"}, 500


rectBp.add_url_rule("/rectmill", view_func=Rect.as_view("RectMill"))
